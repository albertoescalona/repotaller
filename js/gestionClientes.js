var clientesObtenidos;

function getClientes() {
  //var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if (this.readyState ==4 && this.status == 200) {
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

/*icono de su bandera*/
function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var finalRuta = ".png";
  var JSONClientes = JSON.parse(clientesObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divClientes");

  var tabla = document.createElement("table");
  var thead = document.createElement("thead");
  var nuevaFilafirst = document.createElement("tr");
  var th1 = document.createElement("th");
  var th2 = document.createElement("th");
  var th3 = document.createElement("th");
  th1.innerText="ContactName";
  th2.innerText="Country";
  th3.innerText="Flag";
  nuevaFilafirst.appendChild(th1);
  nuevaFilafirst.appendChild(th2);
  nuevaFilafirst.appendChild(th3);
  thead.appendChild(nuevaFilafirst);

  var tBody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText=JSONClientes.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText=JSONClientes.value[i].Country;
    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }else{
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country+ ".png";
    }

    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(imgBandera);
    tBody.appendChild(nuevaFila);
    //tabla.appendChild(nuevaFila);
  }
  tabla.appendChild(thead);
  tabla.appendChild(tBody);
  divTabla.appendChild(tabla);



}
