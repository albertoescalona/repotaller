var productosObtenidos;

function getProductos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      productosObtenidos=request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  request.send();
}


function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");

  var tabla = document.createElement("table");
  var thead = document.createElement("thead");
  var nuevaFilafirst = document.createElement("tr");
  var th1 = document.createElement("th");
  var th2 = document.createElement("th");
  var th3 = document.createElement("th");
  th1.innerText="ProductName";
  th2.innerText="UnitPrice";
  th3.innerText="UnitsInStock";
  nuevaFilafirst.appendChild(th1);
  nuevaFilafirst.appendChild(th2);
  nuevaFilafirst.appendChild(th3);
  thead.appendChild(nuevaFilafirst);
  var tBody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText=JSONProductos.value[i].ProductName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText=JSONProductos.value[i].UnitPrice;
    var columnaStock = document.createElement("td");
    columnaStock.innerText=JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tBody.appendChild(nuevaFila);
    //tabla.appendChild(nuevaFila);
  }
  tabla.appendChild(thead);
  tabla.appendChild(tBody);
  divTabla.appendChild(tabla);
}
